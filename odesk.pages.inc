<?php
/**
 * @file odesk.module
 * TODO: Enter file description here.
 */

/**
 * callback page
 */
function odesk_page($date = FALSE) {
  $params = odesk_ops('time');
  if (!$date) $date = date('Y-m-d');
  $params['tq'] = sprintf($params['tq'], $date, $date);
  $r = odesk_init($params);
  #dpm($r);
  return odesk_table($r);
}

/**
 * Table, created using the 'time' service.
 */
function odesk_table(&$data) {
  $table = (object) $data;
  $data = (object) $data;

  if (isset($data->table)) {
    $table = (object) $data->table;
  }
  if (!isset($table->cols)) {
    t('Did not find table header information');
    return FALSE;
  }
  $header = array();
  $types = array();
  foreach ($table->cols as $col_i => $col) {
    $col = (object) $col;
    $header[] = $col->label;
    $types[] = $col->type;
  }
  $rows = array();
  foreach ($table->rows as $i => $row) {
    foreach ($row->c as $type_i => $value) {
      $type = $types[$type_i];
      if ('date' == $type) {
        $date = new DateTime($value->v);
        $value->v = $date->format('Y-m-d');
      }
      if ('number' == $type) {
        if (!isset($numbers[$type_i])) $numbers[$type_i] = 0;
        $numbers[$type_i] += $value->v;
      }
      $rows[$i][] = $value->v;
    }
  }

  // if it's a number provide a total
  $numbers += array_fill(0, $col_i, '');
  ksort($numbers);

  $rows[] = $numbers;

  $variables = array(
    'header' => $header,
    'rows' => $rows,
  );

  return theme('table', $variables);
}

/**
 * Build up basic info to use odesk api services
 */
function odesk_init($params) {
  if ($cache = odesk_save()) {
    return $cache;
  }
  $lib = libraries_get_path('odesk') . '/oDeskAPI.lib.php';
  if (!file_exists($lib)) {
    return t("Missing library @lib", array('@lib' => $lib));
  }
  include_once($lib);
  $odesk_user = variable_get('odesk_user', ''); // login at odesk
  $odesk_pass = variable_get('odesk_pass', ''); // password at odesk, this unsecure way using pass in script
  $secret     = variable_get('odesk_secret', ''); // your secret key, keep it secure
  $api_key    = variable_get('odesk_api_key', ''); // your application key

  $basepath = variable_get('odesk_url', 'https://www.odesk.com/');

  $api = new oDeskAPI($secret, $api_key);
  // proxy settings
  $proxy = variable_get('proxy_server', 0);
  $proxy_port = variable_get('proxy_port', 0);
  if ($proxy && $proxy_port) {
    $api->option('proxy', $proxy . ':' . $proxy_port);
    $api->option('verify_ssl', FALSE);
  }

  #odesk_session(&$api);
  $api->auth(); // auth process: here your app will be redirected to odesk,

  $url = $basepath . $params['url'];
  unset($params['url']);
  $response = $api->get_request($url, $params);
  $data = json_decode($response);
  odesk_save($data);
  return $data;
}

/**
 *
 */
function odesk_save($cache_data = FALSE) {
  return FALSE;// purge

  if ($cache_data) {
    cache_set('odesk_cache', $cache_data);
    return TRUE;
  }
  else {
    return cache_get('odesk_cache')->data;
  }
}

/**
 *
 */
function odesk_ops($service) {
  $company = variable_get('odesk_company', 'sundaysenergy'); // your company's name
  $team = variable_get('odesk_team', 'sundaysenergy');

  $ops = array();
  $ops['rm'] = array(
    'url' => 'api/team/v1/teamrooms/' . $company . '.json',
  );
  $ops['time'] = array(
    'url' => 'gds/timereports/v1/companies/' . $company . '/teams/' . $team,
    'tq' => "SELECT worked_on, provider_id, provider_name, task, memo, hours, charges WHERE worked_on >= '%s' AND worked_on <= '%s'",
    'tqx' => 'out:json',//'out:csv',//'out:xml',
  );
  $ops['img'] = array(
    'url' => 'api/team/v1/workdiaries/{company}/' . $company . '/2011-11-07' . '{date}.{format}',
  );

  if (isset($ops[$service])) {
    return $ops[$service];
  }
  // service not defined
  drupal_set_message(t('Service (@service) not defined', array('@service' => $service)));
  return FALSE;
}

/**
 *
 */
function odesk_session(&$api) {
  // setup options
  #$api->option('mode', 'nonweb'); // obligatory option for non web-based applications
  #$api->option('verify_ssl', TRUE); // whether to verify SSL certificate, FALSE by default (supports self-signed certs)
  #$api->option('cookie_file', '/tmp/cookies.txt'); // cookie file, used for nonweb apps
  #$api->option('proxy_pwd', 'username:password'); // credentials for proxy, which requires auth
  
  if (!isset($_SESSION['odesk_token_id']) && isset($_GET['frob'])) {
    $token = $_GET['frob'];
    #$token = $api->auth($odesk_user, $odesk_pass); // auth using your login and pass to authorize app
    $_SESSION['odesk_token_id'] = $token; // save your token using prefered method
    $api->option('api_token', $token); // use saved token, then you app do not require
  }
  elseif (isset($_SESSION['odesk_token_id'])) {
    $api->option('api_token', $_SESSION['odesk_token_id']);
  }
}